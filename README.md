## Demo Intro K8s Binar Academy

Apply these manifest :

```
kubectl apply -f manifests/
```

This will create :

- 4 pods with image `gcr.io/kuar-demo/kuard-amd64:blue` in the `deployment.yaml`
- Expose the demo app to the internal cluster using `service.yaml`
- Create external load balancer (GCLB) using `ingress.yaml` in GCP. Only HTTP.

After that, get the public IP of your load balancer using :

```
kubectl get ingress
```